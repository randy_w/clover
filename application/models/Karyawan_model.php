<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model{

    function getAll(){
        $this->db->select('*'); 
        $this->db->from('tuser');
        $this->db->join('tkaryawan', 'tkaryawan.id_user = tuser.id');
        $query = $this->db->get();
        return $query->result();
    }
    function add($user,$karyawan){
        $this->db->insert('tuser',$user);
        $result = $this->db->insert('tkaryawan',$karyawan);
        return $result;
    }
    function getKaryawanById($id){
        $this->db->select('*'); 
        $this->db->from('tuser');
        $this->db->join('tkaryawan', 'tkaryawan.id_user = tuser.id');
        $this->db->where('tkaryawan.id', $id);
        $query = $this->db->get();
        return $query->result();
    }
    function delete($id_user){
        $this->db->where('id_user', $id_user);
        $this->db->delete('tkaryawan');

        $this->db->where('id', $id_user);
        $result =$this->db->delete('tuser');

        return $result;
    }
    function update($user,$karyawan,$id_user){
        $this->db->where('id_user', $id_user);
        $this->db->update('tkaryawan', $karyawan);

        $this->db->where('id', $id_user);
        $result =$this->db->update('tuser', $user);
        return $result;
    }

}