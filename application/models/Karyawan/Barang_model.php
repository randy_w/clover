<?php 
class Barang_model extends CI_Model{
	public function get($id = null){
		// $query = $this->db->query("SELECT * FROM tbarang WHERE id = 1");
		$this->db->select('*');
		$this->db->from('tbarang');
		if ($id != null){
			$this->db->where('id', $id);
		}		
		$query = $this->db->get();
		return $query;
	}

	public function add($data){
		$param = array(
			'id' => $data['id'],
			'nama' => $data['nama'],
			'jenis' => $data['jenis'],
			'harga' => $data['harga'],
			'stock' => $data['stock']
		);
		$this->db->insert('tbarang', $param);
	}

	public function edit($data){
		$param = array(
			'id' => $data['id'],
			'nama' => $data['nama'],
			'jenis' => $data['jenis'],
			'harga' => $data['harga'],
			'stock' => $data['stock'],
		);
		$this->db->set($param);
		$this->db->where('id', $data['id']);
		$this->db->update('tbarang');
	}

	public function hapus($id){
		// $this->db->delete('tbarang', array('id' => $id));
		$this->db->where('id', $id);
		$this->db->delete('tbarang');
	}
}

//f
 ?>