<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model{
    private $tuser = "tuser";
    private $tcustomer = "tcustomer";

    function insert($user,$customer){
        $this->db->insert($this->tuser,$user);
        $this->db->insert($this->tcustomer,$customer);
        if($this->db->affected_rows() == 1) {
                return true;
            } else {
                return false;
            }
    }
    function auth($user,$pass){
        $this->db->where('username',$user);
        $this->db->where('password',$pass);

        return $this->db->get($this->tuser)->row();
    }
}