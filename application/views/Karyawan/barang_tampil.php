<title><?= $header ?></title>
<style type="text/css">
	table.data{
		border-collapse: collapse;
	}

	table.data th, table.data td{
		padding: 5px;
	}

</style>

<div style="margin-bottom: 20px">
	<a href="<?=base_url('karyawan/barang/add');?>"><button>Tambah</button></a>
</div>

<table class="data" border="1">
	<tr>
		<th>No</th>
		<th>id</th>
		<th>nama</th>
		<th>jenis</th>
		<th>harga</th>
		<th>stock</th>
		<th></th>
	</tr>
	<?php 
	$no = 1;
	foreach ($barang as $b =>$row){ ?>
		<tr>
			<td><?=$no++;  ?></td>
			<td><?=$row->id; ?></td>
			<td><?=$row->nama; ?></td>
			<td><?=$row->jenis; ?></td>
			<td><?=$row->harga;  ?></td>
			<td><?=$row->stock;  ?></td>
			<td>
				<a href="<?=base_url('Karyawan/barang/edit/'.$row->id); ?>" ><button>Edit</button></a>
				<a href="<?=base_url('Karyawan/barang/hapus/'.$row->id);  ?>" onclick="return confirm('Yakin ingin menghapus Data?')"><button>Hapus</button></a>
			</td>
		</tr>			
	<?php
		}
	 ?>
</table>

