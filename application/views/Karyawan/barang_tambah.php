<title><?=$header?></title>
<?php echo form_open('karyawan/barang/proses'); ?>
	<table>
		<tr>
			<td>id</td>
			<td>:</td>
			<td>
				<?php echo form_input('id', '', 'required=""'); ?>
			</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td>
				<?php echo form_input('nama', '', 'required=""'); ?>
			</td>
		</tr>
		<tr>
			<td>Jenis</td>
			<td>:</td>
			<td>
				<?php echo form_input('jenis', '', 'required=""'); ?>
			</td>
		</tr>
		<tr>
			<td>Harga</td>
			<td>:</td>
			<td>
				<?php echo form_input(array('type' => 'number', 'name' => 'harga', 'required'=> 'required')); ?>
			</td>
		</tr>
		<tr>
			<td>Stock</td>
			<td>:</td>
			<td>
				<?php echo form_input(array('type' => 'number', 'name' => 'stock', 'required' => 'required')); ?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>
				<?php echo form_input(array('type' => 'submit', 'name' => 'add', 'value' => 'Tambah')); ?>
			</td>
		</tr>
	</table>
<?php echo form_close(); ?>
