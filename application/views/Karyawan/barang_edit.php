<title><?=$header?></title>
<?php echo form_open('karyawan/barang/proses', '', array('id' => $barang->id)); ?>
	<table>
		<tr>
			<td>id</td>
			<td>:</td>
			<td>
				<?php echo form_input('id', $barang->id, 'readonly' ); ?>
			</td>
		</tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td>
				<?php echo form_input('nama', $barang->nama, 'required=""'); ?>
			</td>
		</tr>
		<tr>
			<td>Jenis</td>
			<td>:</td>
			<td>
				<?php echo form_input('jenis', $barang->jenis, 'required=""'); ?>
			</td>
		</tr>
		<tr>
			<td>Harga</td>
			<td>:</td>
			<td>
				<?php echo form_input(array('type' => 'number', 'value' => $barang->harga, 'name' => 'harga', 'required'=> 'required')); ?>
			</td>
		</tr>
		<tr>
			<td>Stock</td>
			<td>:</td>
			<td>
				<?php echo form_input(array('type' => 'number', 'value' => $barang->stock, 'name' => 'stock', 'required' => 'required')); ?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>
				<?php echo form_input(array('type' => 'submit', 'name' => 'edit', 'value' => 'Edit')); ?>
			</td>
		</tr>
	</table>
<?php echo form_close(); ?>

