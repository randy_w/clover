<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clover</title>
    <link rel="icon" href="<?=base_url('assets/img/icon.png')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/style.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/sweetalert.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <script src="<?=base_url('assets/js/jquery-3.3.1.min.js')?>"></script>
    <script src="<?=base_url('assets/js/sweetalert.min.js')?>"></script>
</head>
<body>
    <div class="cover-bg"></div>
    <div class="content">
        <div class="left">
            <div class="contentLeft">
                <div class="title">Dapatkan bunga yang<br><span class="other">tepat sekarang juga</span></div>
                <div class="subTitle">Pilih berbagai macam jenis bunga</div>
                <div class="list">
                    <ul>
                        <li><span class="listText">Cari & pilih bunga yang kamu suka</span></li>
                        <li><span class="listText">Pesan bunga yang kamu pilih</span></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="right">
            <img src="<?=base_url('assets/img/LogoClover.png');?>" class="logo">
                <button class="button button-register" data-toggle="modal" data-target="#registerModal">REGISTER</button>
                    <p class="wordLine">Already have an account ?</p>
                <button class="button button-login" data-toggle="modal" data-target="#loginModal">LOGIN</button>
        </div>
    </div>
    <!--Register Modal-->
    <div class="modal fade" id="registerModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 style="color: #000">REGISTER</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" style="cursor: pointer">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="contentContainer">
                        <form action="#" id="registerForm" method="post">
                            <div class="inputBox">
                                <input type="email" name="registerEmail" id="registerEmail" autocomplete="off" required>
                                <i class="material-icons">email</i><label>Email</label>
                            </div>
                            <div class="inputBox">
                                <input type="text" name="registerUsername" id="registerUsername" autocomplete="off" required>
                                <i class="material-icons">person</i><label>Username</label>
                            </div>
                            <div class="inputBox">
                                <input type="password" name="registerPassword" id="registerPassword" required>
                                <i class="material-icons">lock</i><label>Password</label>
                            </div>
                                <button type="submit" name="submit" value="REGISTER" class="submit">REGISTER</button>
                                <p></p>
                                <!-- <p><br>Already have an account? <a href="#">Login</a></p> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Login Modal-->
    <div class="modal fade" id="loginModal" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 style="color: #000">LOGIN</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="cursor: pointer">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                            <div class="contentContainer">
                                <form action="#" id="loginForm" method="post">
                                    <div class="inputBox">
                                        <input type="text" name="loginUsername" id="loginUsername" autocomplete="off" required>
                                        <i class="material-icons">person</i><label>Username</label>
                                    </div>
                                    <div class="inputBox">
                                        <input type="password" name="loginPassword" id="loginPassword" required>
                                            <i class="material-icons">lock</i><label>Password</label>
                                    </div>
                                    <button type="submit" name="submit" value="LOGIN" class="submit" id="Login">LOGIN</button>
                                    <p></p>
                                    <!-- <p><br>Doesn't have an account? <a href="#">Register</a></p> -->
                                </form>
                            </div>
                        </div>
                        <!-- <div class="modal-footer">
                            <div class="container">
                                <div class="row">
                                    <span class="copyright">Copyright Ⓒ Clover. All Right Reserved</span>
                                </div>
                            </div>
                        </div> -->
                </div>
            </div>
        </div>
    </div>
    <script src="<?=base_url('assets/js/popper.min.js')?>"></script>
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
    <script>
        var BASEURL = "<?= base_url() ?>";
        $(function(){
            $('.alert').alert();
            $('#registerForm').on('submit',function(event){
                event.preventDefault();
                
                //Get from value
                var data = {
                    'username' : $("input[id='registerUsername']").val(),
                    'password' : $("input[name='registerPassword']").val(),
                    'email'    : $("input[name='registerEmail']").val(),
                }
                //Ajax Request to auth/save
                $.ajax({
                    type:'POST',
                    url: BASEURL + "/auth/save",
                    data:data,
                    success:function(res){
                        //Reset form value
                        $("input[id='registerUsername']").val('');
                        $("input[name='registerPassword']").val('');
                        $("input[name='registerEmail']").val('');
                        //Closing modal
                        $('#registerModal').modal('hide');
                        if(res){
                            swal({
                                title: "Success",
                                text: 'Selamat Anda Telah Berhasil Mendaftar',
                                type: "success",
                                showConfirmButton: true
                            });
                        }else{
                            swal({
                                title: "Error",
                                text: 'Mohon Maaf Terjadi Kesalahan',
                                type: "error",
                                showConfirmButton: true
                            }); 
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //Log error to console
                        console.log(xhr.responseText);
                    }
                });
            });
            
            $('#loginForm').on('submit',function(event){
                event.preventDefault();
                var data = {
                    'username' : $("input[id='loginUsername']").val(),
                    'password' : $("input[name='loginPassword']").val(),
                    'submit' : $("#Login").val()
                }
                console.log(data);
                $.ajax({
                    type:'POST',
                    url: BASEURL + "auth/login",
                    data:data,
                    success:function(res){
                        var result = JSON.parse(res);
                        console.log(result);
                        if(result.status){
                            if(result.login){
                                $('#loginModal').modal('hide');
                                // swal({
                                //     title: "Success",
                                //     text: 'Selamat Datang '+result.username,
                                //     type: "success",
                                //     showConfirmButton: true
                                // });
                                window.location = BASEURL + 'auth/dashboard';
                            }else{
                                swal({
                                    title: "Error",
                                    text: result.msg,
                                    type: "error",
                                    showConfirmButton: true
                                });
                            }
                        }else{
                            swal({
                                title: "Error",
                                text: result.msg,
                                type: "error",
                                showConfirmButton: true
                            }); 
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //Log error to console
                        console.log(xhr.responseText);
                    }
                });
            }); 
            
        });
    </script>
</body>
</html>
