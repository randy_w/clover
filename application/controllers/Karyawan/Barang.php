<?php 
defined('BASEPATH') OR exit ('No script access allowed');

/**
 * 
 */
class Barang extends CI_Controller
{
	function __construct(){
		parent:: __construct();
		$this->load->model('Karyawan/Barang_model', 'Barang_model');
	}

	public function index(){
		$query = $this->Barang_model->get();
		// $data['header'] = 'Tampil_Barang';
		// $data['barang'] = $query->result();
		$data = array(
			'header' => 'Tampil_barang',
			'barang' => $query->result(),
		);
		$this->load->view('karyawan/barang_tampil', $data);
	}

	public function add(){
		$data = array(
			'header' => 'Tambah_barang'
		);
		$this->load->view('karyawan/barang_tambah', $data);
	}

	public function edit($id = null){
		$query = $this->Barang_model->get($id);
		$data = array(
			'header' => 'Edit_barang',
			'barang' => $query->row()
		);
		$this->load->view('karyawan/barang_edit', $data);
	}

	public function proses(){
		if(isset($_POST['add'])){
			// $id = $this->input->post('id');
			// $nama = $this->input->post('nama');
			// echo $id."<br>".$nama;
			$inputan = $this->input->post(null, TRUE);
			$this->Barang_model->add($inputan);
		}
		else if(isset($_POST['edit'])){
			$inputan = $this->input->post(null, TRUE);
			$this->Barang_model->edit($inputan);
		}
		redirect('karyawan/barang');
	}

	public function hapus($id){
		$this->Barang_model->hapus($id);
		redirect('karyawan/barang');
	}
}

//f
 ?>