<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('Karyawan_model','karyawan');
    }
    public function index(){
        
    }
    public function getKaryawan(){
        $data = $this->karyawan->getAll();
        echo json_encode($data);
    }
    public function addKaryawan(){
        $newUserId = substr(md5(uniqid(rand(), true)),-5);
        $newKaryawanId = substr(md5(uniqid(rand(), true)),-5);
        $user = array(
            'id' => $newUserId,
            'username'  => $this->input->post('username'), 
            'password'  => $this->input->post('password'),  
            'level' => 2
        );
        $karyawan = array(
            'id' => $newKaryawanId,
            'id_user' => $newUserId,
            'nama' => $this->input->post('nama'),
            'no_telp'  => $this->input->post('no_telp'), 
            'email'  => $this->input->post('email'),
            'alamat' =>  $this->input->post('alamat')
        );
        $data= $this->karyawan->add($user,$karyawan);
        if($data){
            $response = array(
                'status' => true,
                'id' => $karyawan['id'],
                'id_user' => $user['id'],
                'username'  => $this->input->post('username'), 
                'password'  => $this->input->post('password'), 
                'nama' => $this->input->post('nama'),
                'no_telp'  => $this->input->post('no_telp'), 
                'email'  => $this->input->post('email'),
                'alamat' =>  $this->input->post('alamat')
            );
        }else{
            $response = array(
                'status' => false,
                'msg' => 'Mohon Maaf Terjadi Kesalah' 
            );
        }
        echo json_encode($response);
    }
    public function readKaryawan(){
        $id = $this->input->post('id');
        $data = $this->karyawan->getkaryawanById($id);
        echo json_encode($data);
    }
    public function deleteKaryawan(){
        $id_user = $this->input->post('id_user');
        $result = $this->karyawan->delete($id_user);
        
        if($result){
            $response = array(
                'status' => true,
                'msg' => 'Karayawn Berhasil Di Hapus',
                'id_user' => $id_user
            );
        }else{
            $response = array(
                'status' => false,
                'msg' => 'Karayawn Gagal Di Hapus',
                'id_user' => $id_user
            );
        }

        echo json_encode($response);
    }
    public function updateKaryawan(){
        $id_user = $this->input->post('id_user');
        $id = $this->input->post('id');
        $user = array(
            'username'  => $this->input->post('username'), 
            'password'  => $this->input->post('password')
        );
        $karyawan = array(
            'no_telp'  => $this->input->post('no_telp'), 
            'email'  => $this->input->post('email'),
            'nama' => $this->input->post('nama'),
            'alamat' => $this->input->post('alamat') 
        );
        $query = $this->karyawan->update($user,$karyawan,$id_user);
        if($query){
            $response = array(
                'status' => true,
                'id' => $id,
                'id_user' => $id_user,
                'username'  => $this->input->post('username'), 
                'password'  => $this->input->post('password'), 
                'nama' => $this->input->post('nama'),
                'phone'  => $this->input->post('no_telp'), 
                'email'  => $this->input->post('email'),
                'alamat'  => $this->input->post('alamat') 
            );
        }else{
            $response = array(
                'status' => true,
                'msg' => 'Mohon Maaf Terjadi Kesalahan'
            );
        }
        echo json_encode($response);
    }
}