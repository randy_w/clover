<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
		$this->load->model('Auth_model','auth');
	}		
	public function index()
	{
		$this->load->view('index');
	}
	public function save(){
		$post = $this->input->post();
		$userid = md5(uniqid(rand(), true));//Generate uniq id
		$user = array(
			'id' => $userid,
            'username' => $post['username'],
            'password' => $post['password'],
            'level' => 3
		);
		$customer = array(
            'id' => md5(uniqid(rand(), true)),
            'id_user' => $userid,
            'email' => $post['email']
        );
		$res = $this->auth->insert($user,$customer);
		if($res){
			$status = true;
		}else{
			$status = false;
		}
		echo json_encode($status);
	}
	public function login(){
		if(isset($_POST['submit'])){
			$user = $this->input->post('username');
			$pass = $this->input->post('password');

			$query = $this->auth->auth($user,$pass);
			$row = count($query);
			if($row >0){
				$login = $this->db->get_where('tuser', array('username' => $user,'password' => $pass))->row();
				$data = array(
                    'isLogin' => true,
                    'username' => $login->username,
                    'level' => $login->level
				);
				$this->session->set_userdata($data);
				$response = array(
					'status' => true,
					'login' =>true,
					'msg' => 'Login Berhasil',
					'username' => $this->session->username,
					'level' => $this->session->level
				);
			}else{
				$response = array(
					'status' => true,
					'login' => false,
					'msg' => 'Username Atau Password Salah',
				);
			}
			
		}else{
			$response = array(
				'status' => false,
				'msg' => 'Submit Error'
			);
		}
		echo json_encode($response);
	}

	public function dashboard() {
		$this->load->view('dashboard');
	}
	
}
