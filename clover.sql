-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03 Okt 2018 pada 15.19
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clover`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tadmin`
--

CREATE TABLE `tadmin` (
  `id` varchar(4) NOT NULL,
  `id_user` varchar(4) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbarang`
--

CREATE TABLE `tbarang` (
  `id` varchar(4) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `harga` bigint(8) NOT NULL,
  `stock` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tcustomer`
--

CREATE TABLE `tcustomer` (
  `id` varchar(4) NOT NULL,
  `id_user` varchar(4) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kode_post` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tdetailpemesanan`
--

CREATE TABLE `tdetailpemesanan` (
  `id_detailpemesanan` varchar(4) NOT NULL,
  `id_detailbarang` varchar(4) NOT NULL,
  `qty` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tkaryawan`
--

CREATE TABLE `tkaryawan` (
  `id` varchar(4) NOT NULL,
  `id_user` varchar(4) NOT NULL,
  `nama` varchar(32) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tkeranjang`
--

CREATE TABLE `tkeranjang` (
  `id` varchar(4) NOT NULL,
  `id_pemesanan` varchar(4) NOT NULL,
  `qty` int(3) NOT NULL,
  `total_harga` bigint(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpembayaran`
--

CREATE TABLE `tpembayaran` (
  `id` varchar(4) NOT NULL,
  `id_pemesanan` varchar(4) NOT NULL,
  `id_karyawan` varchar(4) NOT NULL,
  `total_harga` bigint(8) NOT NULL,
  `tgl_bayar` datetime NOT NULL,
  `status_bayar` enum('Dibayar','Belum Dibayar','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tpemesanan`
--

CREATE TABLE `tpemesanan` (
  `id` varchar(4) NOT NULL,
  `id_customer` varchar(4) NOT NULL,
  `tgl_pemesanan` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tuser`
--

CREATE TABLE `tuser` (
  `id` varchar(4) NOT NULL,
  `username` varchar(12) NOT NULL,
  `password` varchar(12) NOT NULL,
  `level` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tadmin`
--
ALTER TABLE `tadmin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `useradmin` (`id_user`);

--
-- Indexes for table `tbarang`
--
ALTER TABLE `tbarang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tcustomer`
--
ALTER TABLE `tcustomer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usercustomer` (`id_user`);

--
-- Indexes for table `tdetailpemesanan`
--
ALTER TABLE `tdetailpemesanan`
  ADD KEY `detailbeli` (`id_detailpemesanan`),
  ADD KEY `detailbarang` (`id_detailbarang`);

--
-- Indexes for table `tkaryawan`
--
ALTER TABLE `tkaryawan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userkaryawan` (`id_user`);

--
-- Indexes for table `tkeranjang`
--
ALTER TABLE `tkeranjang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `keranjang_pemesanan` (`id_pemesanan`);

--
-- Indexes for table `tpembayaran`
--
ALTER TABLE `tpembayaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawanbayar` (`id_karyawan`),
  ADD KEY `bayarbeli` (`id_pemesanan`);

--
-- Indexes for table `tpemesanan`
--
ALTER TABLE `tpemesanan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pembeliancustomer` (`id_customer`);

--
-- Indexes for table `tuser`
--
ALTER TABLE `tuser`
  ADD PRIMARY KEY (`id`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tadmin`
--
ALTER TABLE `tadmin`
  ADD CONSTRAINT `useradmin` FOREIGN KEY (`id_user`) REFERENCES `tuser` (`id`);

--
-- Ketidakleluasaan untuk tabel `tcustomer`
--
ALTER TABLE `tcustomer`
  ADD CONSTRAINT `usercustomer` FOREIGN KEY (`id_user`) REFERENCES `tuser` (`id`);

--
-- Ketidakleluasaan untuk tabel `tdetailpemesanan`
--
ALTER TABLE `tdetailpemesanan`
  ADD CONSTRAINT `detailbarang` FOREIGN KEY (`id_detailbarang`) REFERENCES `tbarang` (`id`),
  ADD CONSTRAINT `detailbeli` FOREIGN KEY (`id_detailpemesanan`) REFERENCES `tpemesanan` (`id`);

--
-- Ketidakleluasaan untuk tabel `tkaryawan`
--
ALTER TABLE `tkaryawan`
  ADD CONSTRAINT `userkaryawan` FOREIGN KEY (`id_user`) REFERENCES `tuser` (`id`);

--
-- Ketidakleluasaan untuk tabel `tkeranjang`
--
ALTER TABLE `tkeranjang`
  ADD CONSTRAINT `keranjang_pemesanan` FOREIGN KEY (`id_pemesanan`) REFERENCES `tpemesanan` (`id`);

--
-- Ketidakleluasaan untuk tabel `tpembayaran`
--
ALTER TABLE `tpembayaran`
  ADD CONSTRAINT `bayarbeli` FOREIGN KEY (`id_pemesanan`) REFERENCES `tpemesanan` (`id`),
  ADD CONSTRAINT `karyawanbayar` FOREIGN KEY (`id_karyawan`) REFERENCES `tkaryawan` (`id`);

--
-- Ketidakleluasaan untuk tabel `tpemesanan`
--
ALTER TABLE `tpemesanan`
  ADD CONSTRAINT `pembeliancustomer` FOREIGN KEY (`id_customer`) REFERENCES `tcustomer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
